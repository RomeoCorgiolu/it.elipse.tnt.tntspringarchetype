[UpdateLocalCatalog.sh](https://bitbucket.org/RomeoCorgiolu/it.elipse.tnt.tntspringarchetype/src/65f84a7ed831a714802dd30fd3854597d610b213/script/UpdateLocalCatalog.sh?at=master&fileviewer=file-view-default)
## TNT Spring MVC Quickstart Maven Archetype

### Summary

The project is a Maven archetype for TNT Spring MVC web application.

### Generated project characteristics

- No-xml Spring MVC web application
- Thymeleaf, Bootstrap
- JPA (Hibernate/HSQLDB/Spring Data JPA)
- JUnit/Mockito
- Spring Security
- MongoDB (Spring Data Mongo)

### Prerequisites

- JDK 8
- Maven 3

### Update Archetype Catalog

[UpdateLocalCatalog.sh](https://bitbucket.org/RomeoCorgiolu/it.elipse.tnt.tntspringarchetype/src/65f84a7ed831a714802dd30fd3854597d610b213/script/UpdateLocalCatalog.sh?at=master&fileviewer=file-view-default)

### Create a project
Ensure you have the archetype on your local catalog
```bash
	cat ~/.m2/repository/archetype-catalog.xml
```
```xml
	<archetypes>
	    <archetype>
	      <groupId>it.elipse.tnt</groupId>
	      <artifactId>tntspringarchetype-archetype</artifactId>
	      <version>0.0.1</version>
	      <description>tntspringarchetype-archetype</description>
	    </archetype>
	</archetypes>
```
```bash
    mvn archetype:generate \
		-DarchetypeGroupId=it.elipse.tnt \
        -DarchetypeArtifactId=tntspringarchetype-archetype \
        -DarchetypeVersion=0.0.1\
        -DgroupId=my.groupid \
        -DartifactId=my-artifactId \
        -Dversion=version    
```
### Run the project

Navigate to newly created project directory (my-artifactId) and then run:

```bash
	mvn initialize
	mvn test tomcat7:run
```

### Test in the browser

http://localhost:8080/my-artifactId

Note: No additional services are required in order to start the application. Mongo DB configuration is in place but it is not used in the code.


### Switching to PostgreSQL

- Add dependency to PostgreSQL driver in POM:
```xml
<dependency>
    <groupId>org.postgresql</groupId>
    <artifactId>postgresql</artifactId>
    <version>9.4.1207</version>
</dependency>`
```
- Change 

persistence.properties:
```properties
dataSource.driverClassName=org.postgresql.Driver
dataSource.url=jdbc:postgresql:postgres
dataSource.username=postgres
dataSource.password=postgres

hibernate.dialect=org.hibernate.dialect.PostgreSQL9Dialect
hibernate.hbm2ddl.auto=create
hibernate.show_sql=true
hibernate.format_sql=true
hibernate.use_sql_comments=true
```
### Enabling MongoDB repositories

- Open MongoConfig class and uncomment the following line:
```java
// @EnableMongoRepositories(basePackageClasses = Application.class)
```
Now you can add repositories to your project:
```java
@Repository
public interface MyRepository extends MongoRepository&lt;MyDocument, String&gt; {

}
```