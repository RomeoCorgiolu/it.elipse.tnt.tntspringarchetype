package it.elipse.tnt.station;

import org.springframework.data.repository.CrudRepository;

public interface StationTypeRepository extends CrudRepository<StationType, String>{

}
