package it.elipse.tnt.station;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "station")
public class Station {

	@Id
	private String id;
	@Column(name="intcode")
	private String intCode;
	private String name;
	@Column(nullable=false)
	private boolean active = true;
	private String latitude;
	private String longitude;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stationtypeid")
	@NotNull
	private StationType stationType;
	
	public Station() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntCode() {
		return intCode;
	}

	public void setIntCode(String intCode) {
		this.intCode = intCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public StationType getStationType() {
		return stationType;
	}

	public void setStationTypeId(StationType stationType) {
		this.stationType = stationType;
	}

	public Station(String id, String intCode, String name, boolean active, String latitude, String longitude,
			StationType stationType) {
		super();
		this.id = id;
		this.intCode = intCode;
		this.name = name;
		this.active = active;
		this.latitude = latitude;
		this.longitude = longitude;
		this.stationType = stationType;
	}
	
	
	
}
