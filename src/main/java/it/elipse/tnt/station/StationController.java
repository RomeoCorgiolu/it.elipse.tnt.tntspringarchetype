package it.elipse.tnt.station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.elipse.tnt.account.RoleEnum;

@Controller
public class StationController {

	@Autowired
	private StationRepository stationRepository;
	
	@Secured(RoleEnum.Constants.ROLE_ADMIN)
	@RequestMapping(value = "station", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("stations",stationRepository.findAll());
        return "station/index";
	}
}
