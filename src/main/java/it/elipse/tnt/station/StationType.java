package it.elipse.tnt.station;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "stationtype")
public class StationType {
	@Id
	private String id;
	private String description;
	
	@OneToMany(mappedBy="stationType")
	private Set<Station> station= new HashSet<Station>();

	public StationType() {
		
	}
	
	public StationType(StationTypeEnum depot,String descritpion) {
		this.id= depot.toString();
		this.description = descritpion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Station> getStations() {
		return station;
	}

	public void setStations(Set<Station> stations) {
		this.station = stations;
	}
}
