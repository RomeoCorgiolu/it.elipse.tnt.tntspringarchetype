package it.elipse.tnt.station;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StationService {

	@Autowired
	private StationRepository stationRepository;
	
	@Autowired
	private StationTypeRepository stationTypeRepository;
	
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Value("#{new Boolean('${project.seedData}')}")
	private boolean seedData;
	
	@PostConstruct
	public void seed() {
		if(!seedData) {
			log.warn("Seed Data Disabled");
			return;
		}
		log.info("Seed stations on db");
		stationTypeRepository.save(new StationType(StationTypeEnum.DEPOT,StationTypeEnum.DEPOT.toString()));
		stationTypeRepository.save(new StationType(StationTypeEnum.HUBAIR,StationTypeEnum.HUBAIR.toString()));
		stationTypeRepository.save(new StationType(StationTypeEnum.HUBROAD,StationTypeEnum.HUBROAD.toString()));
		stationTypeRepository.save(new StationType(StationTypeEnum.HUBROADINT,StationTypeEnum.HUBROADINT.toString()));
		stationTypeRepository.save(new StationType(StationTypeEnum.PARKSHIP,StationTypeEnum.PARKSHIP.toString()));
		
		stationRepository.save(new Station("AG","IAE","Agrigento",true,"37.454250","13.980449",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                 
		stationRepository.save(new Station("AL","QAL","Alessandria",true,"44.893902","8.701691",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                
		stationRepository.save(new Station("AN","AOI","Ancona",true,"43.560238","13.511794",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("AO","AOT","Aosta",true,"45.740971","7.367469",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                      
		stationRepository.save(new Station("APR","QZR","Aprilia",true,"41.579422","12.661504",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                  
		stationRepository.save(new Station("AR","QZO","Arezzo",true,"43.458439","11.826254",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("AT","OAT","Asti",true,"44.906456","8.269220",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                       
		stationRepository.save(new Station("BA","BRI","Bari",true,"41.112740","16.783091",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                      
		stationRepository.save(new Station("BDG","IBD","Bassano Del Grappa",true,"45.778389","11.755900",new StationType(StationTypeEnum.DEPOT,"DEPOT")));       
		stationRepository.save(new Station("BG","BRG","Bergamo",true,"45.649734","9.709929",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("BIE","BEA","Biella",true,"45.558235","8.121078",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("BNS","OS3","Beinasco",true,"45.384720","11.921620",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                 
		stationRepository.save(new Station("BZ","ZBN","Bolzano",true,"46.475918","11.329534",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("CA","CA7","Cagliari Elmas",true,"39.265045","9.074167",new StationType(StationTypeEnum.DEPOT,"DEPOT")));             
		stationRepository.save(new Station("CIV","ICV","Civitanova Marche",true,"43.287701","13.676616",new StationType(StationTypeEnum.DEPOT,"DEPOT")));        
		stationRepository.save(new Station("CN","CUF","Cuneo",true,"44.423058","7.548562",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                      
		stationRepository.save(new Station("CO","ICM","Como",true,"45.776386","9.045938",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                       
		stationRepository.save(new Station("CT","CT1","Catania",true,"37.434902","15.030378",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("EMP","IEM","Empoli",true,"43.705753","10.899849",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("FG","FOG","Foggia",true,"41.454575","15.498336",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("FI","FI1","Firenze Calenzano",true,"43.844311","11.161543",new StationType(StationTypeEnum.DEPOT,"DEPOT")));         
		stationRepository.save(new Station("FNR","FN1","Fiano",true,"42.132267","12.579778",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("FO","FOI","Forli'",true,"44.225330","12.080368",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("FR","QFR","Frosinone",true,"41.628445","13.271806",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                 
		stationRepository.save(new Station("GE2","GOA","Genova",true,"44.415531","8.844859",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("GR","GRS","Grosseto",true,"42.783630","11.098519",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                  
		stationRepository.save(new Station("IM","IIM","Imperia",true,"43.944672","8.002800",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("LCC","ILJ","Lecco",true,"45.844669","9.368565",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("LE","ILC","Lecce",true,"40.388668","18.115154",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("LOD","ILD","Lodi",true,"45.289886","9.538108",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                      
		stationRepository.save(new Station("LU","ILT","Lucca",true,"43.833042","10.521718",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("ME","QME","Messina",true,"38.165375","15.544878",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("MI10","MM1","Milano Mega",true,"45.437828","9.282811",new StationType(StationTypeEnum.DEPOT,"DEPOT")));              
		stationRepository.save(new Station("MN","IMV","Mantova",true,"45.194202","10.771924",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("MNZ","MZ1","Monza",true,"45.589771","9.321144",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("MO","MDA","Modena",true,"44.670376","10.937967",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("NA1","NA1","Napoli Teverola",true,"41.006405","14.224684",new StationType(StationTypeEnum.DEPOT,"DEPOT")));          
		stationRepository.save(new Station("NC3","NC3","Napoli Casoria",true,"40.893738","14.295436",new StationType(StationTypeEnum.DEPOT,"DEPOT")));           
		stationRepository.save(new Station("NO","RNV","Novara",true,"45.453365","8.579908",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("OLB","OLB","Olbia",true,"40.946186","9.518333",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("PA1","PM4","Palermo",true,"38.092907","13.395019",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                  
		stationRepository.save(new Station("PC","QPZ","Piacenza",true,"45.051682","9.732861",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("PE","PE3","Pescara",true,"42.379097","14.145886",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("PER","IPO","Pero",true,"45.512447","9.044878",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                      
		stationRepository.save(new Station("PG","PEG","Perugia",true,"43.106167","12.335999",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("PI","PSA","Pisa",true,"43.678917","10.436306",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                      
		stationRepository.save(new Station("PR","PMF","Parma",true,"44.853168","10.233086",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("PS","IPS","Pesaro",true,"43.886585","12.877794",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("RA","RAN","Ravenna",true,"44.421646","12.148189",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("RE","REM","Reggio Emilia",true,"44.730618","10.614719",new StationType(StationTypeEnum.DEPOT,"DEPOT")));             
		stationRepository.save(new Station("RIM","RMI","Rimini",true,"44.065636","12.465496",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("RM3","ROM","Roma Cinecitta'",true,"41.858833","12.588418",new StationType(StationTypeEnum.DEPOT,"DEPOT")));          
		stationRepository.save(new Station("RM5","RMZ","Roma Est",true,"41.933632","12.624985",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                 
		stationRepository.save(new Station("SA","QSR","Salerno",true,"40.640457","14.910043",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("SBT","ISZ","San Benedetto Del Tronto",true,"42.898674","13.864654",new StationType(StationTypeEnum.DEPOT,"DEPOT"))); 
		stationRepository.save(new Station("SI","ISU","Siena Monteriggioni",true,"43.383232","11.273689",new StationType(StationTypeEnum.DEPOT,"DEPOT")));       
		stationRepository.save(new Station("SP","QLP","La Spezia",true,"44.115490","9.864900",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                  
		stationRepository.save(new Station("SS","QSS","Sassari",true,"40.729778","8.530873",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("TA","TAR","Taranto",true,"40.524891","17.261127",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("TN","ZIA","Trento",true,"46.131001","11.108676",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("TO1","TO1","Torino San Mauro",true,"45.121998","7.753049",new StationType(StationTypeEnum.DEPOT,"DEPOT")));          
		stationRepository.save(new Station("TRM","ITL","Termoli",true,"41.954273","15.011999",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                  
		stationRepository.save(new Station("TS","ITC","Trieste",true,"45.714504","13.742425",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("TV","TV1","Treviso",true,"45.711735","12.206410",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("UA1","UA1","Portogruaro",true,"45.780853","12.792918",new StationType(StationTypeEnum.DEPOT,"DEPOT")));              
		stationRepository.save(new Station("UD","UDN","Udine",true,"46.095352","13.244307",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("VA","QVA","Varese",true,"45.843723","8.831281",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                     
		stationRepository.save(new Station("VE","VE1","Venezia",true,"45.547043","12.291107",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("VI","VNZ","Vicenza",true,"45.516689","11.494808",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("VR1","VRN","Verona",true,"45.390987","10.986275",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("VT","IVT","Viterbo",true,"42.446667","12.092595",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("ZD1","ZD1","Zibido",true,"45.357471","9.131493",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("CA14","CAG","Cagliari Hub Air",true,"44.533424","11.281044",new StationType(StationTypeEnum.HUBAIR,"HUBAIR")));      
		stationRepository.save(new Station("OME","IOE","Omegna",true,"45.437828","9.282811",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                    
		stationRepository.save(new Station("RC","REG","Reggio Calabria",true,"45.383923","11.920933",new StationType(StationTypeEnum.DEPOT,"DEPOT")));           
		stationRepository.save(new Station("SV","ISV","Savona",false,"41.799419","12.590521",new StationType(StationTypeEnum.DEPOT,"DEPOT")));                   
		stationRepository.save(new Station("VE2","VE2","Venezia Posto Barche",true,"45.066822","7.671054",new StationType(StationTypeEnum.PARKSHIP,"PARKSHIP")));	}
}
