package it.elipse.tnt.config;

import java.nio.file.ProviderNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.tnt.connection.SecurityServiceImpl;

import it.elipse.tnt.account.Account;
import it.elipse.tnt.account.AccountRepository;

@Component
public class TntAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	private AccountRepository accountRepository;
	
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
  
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();
         
        if (shouldAuthenticateAgainstThirdPartySystem(userName, password)) {
            // use the credentials
            // and authenticate against the third-party system
            return new UsernamePasswordAuthenticationToken(userName, password,authentication.getAuthorities());
        } else {
        	throw new BadCredentialsException("External system authentication failed");
        }
    }
 
    private boolean shouldAuthenticateAgainstThirdPartySystem(String userName, String password) {
    	Account account = accountRepository.findById(userName);
		if(account == null) {
			throw new UsernameNotFoundException("user not found");
		}
		
		if(userName.equals("user") && !password.equals("demo")) {
			return false;
		}
		
		if(!userName.equals("user") && !userName.equals("admin"))
		{
			SecurityServiceImpl service = new SecurityServiceImpl();
			try {
		        String result = service.authenticate(userName, password);
				if(!result.startsWith("00")) {
					throw new BadCredentialsException(result);
				}
			} catch (Exception e) {
				throw new ProviderNotFoundException("Impossible connect to authentication provider");
			}
		}
		return true;
	}

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}