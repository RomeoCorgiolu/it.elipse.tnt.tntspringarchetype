package it.elipse.tnt.account;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "account")
@NamedQuery(name = Account.FIND_BY_ID, query = "select a from Account a where a.id = :id")
public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9166449111775189259L;

	public static final String FIND_BY_ID = "Account.findById";

	@Id
	private String id;
	
	@JsonIgnore
	private String password;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleid")
	@NotNull
	private Role role;

    protected Account() {

	}
	
	public Account(String id, String password, Role role) {
		this.id = id;
		this.password = password;
		this.setRole(role);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
