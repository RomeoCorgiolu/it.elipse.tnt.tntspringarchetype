package it.elipse.tnt.account;

public enum RoleEnum {
	ADMIN(Constants.ROLE_ADMIN ),USER(Constants.ROLE_USER);
	private final String role;
	
	RoleEnum(String role) {
		this.role = role;
	}
	
	public String getRole() {
		return role;
	}
	
    public static class Constants {
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
    }
}
