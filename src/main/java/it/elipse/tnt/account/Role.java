package it.elipse.tnt.account;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "role")
public class Role implements Serializable{
	/**
	 * 
	 */
	
	@JsonIgnore
	private static final long serialVersionUID = 8330697731787658849L;

	@Id
	private String id = RoleEnum.USER.getRole();
	
	private String description;
	
	@OneToMany(mappedBy="role")
	private Set<Account> account = new HashSet<Account>();

	public Role() {
		this.id = RoleEnum.USER.getRole();
		this.description = this.id;
	}
	
	public Role(RoleEnum role) {
		this.id = role.getRole();
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(RoleEnum role) {
		this.id = role.getRole();
	}	
	
	public Set<Account> getAccount() {
		return this.account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
